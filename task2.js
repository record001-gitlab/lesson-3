// 1- task

function check(num) {
  if (num % 3 == 0 && num % 5 == 0) {
    return num + " is divisable by 3 and 5 ";
  } else {
    return num + " is NOT divisable by 3 and 5 ";
  }
}

let result = check(15);
// console.log(result);

//2-task

function check_odd_even(val) {
  if (val % 2 == 0) {
    return val + " is even number";
  } else {
    return val + " is odd number";
  }
}

// console.log(check_odd_even(7));

// 3-task
let arr = [4, 2, 7, 9, 1];

function sort_array(arr) {
  return arr.sort((a, b) => a - b);
}

// console.log(sort_array(arr));

// 4-task

let arr1 = [
  { test: ["a", "b", "c", "d"] },
  { test: ["a", "b", "c"] },
  { test: ["a", "d"] },
  { test: ["a", "b", "k", "e", "e"] },
];

function find_unique(arr) {
  let result_arr = [];
  arr.forEach((element) => {
    element.test.forEach((item) => {
      !result_arr.includes(item) ? result_arr.push(item) : null;
    });
  });

  return result_arr;
}

// console.log( find_unique(arr1));

// 5 - task

let obj1 = { a: 4, b: 5 };
let obj2 = { x: 4, b: 1 };

function compare_objects(obj1, obj2) {
  
  let value1 = Object.values(obj1).toString();
  let value2 = Object.values(obj2).toString();

  return value1===value2;

  
}

// console.log(compare_objects(obj1, obj2));
