// 1-task

let output = []
for (let i = 1; i <= 100; i++) {
  
  
  if(i % 3 ==0 && i % 5 ==0){
    output.push( "FizzBuzz" + i)
  }else if(i%3 ==0 ) {

    output.push( "Fizz"+ i)
  }else if(i% 5== 0) {

    output.push( "Buzz"+ i)
  }
}

// console.log(output);

//  2-task

function filter_arr(arr) {
  let filtered_arr = []
  let sorted_arr = []
  arr.forEach(element => {
      Array.isArray(element) ? filtered_arr.push(element) : null;
  });
  
  filtered_arr.flat(Infinity).forEach(item => {
    !sorted_arr.includes(item) ? sorted_arr.push(item) : null
  })
  return sorted_arr.sort((a,b) => a-b)
}
let x =[ [2], 23, 'dance', true, [3, 5, 3], [65, 45] ]
console.log(filter_arr(x));